# Spring Boot Demo Application

* Demonstrates `@ConfigurationProperties`: see `GreetingProperties` class for configurable properties

Example customization:
`java -jar spring-boot-demo-0.0.1-SNAPSHOT.jar '--greeting.template=Hi %s'`


