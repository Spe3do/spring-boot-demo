package hu.dpc.edu.springbootdemo.repository;


import hu.dpc.edu.springbootdemo.entity.Book;
import hu.dpc.edu.springbootdemo.helper.querydsl.QueryDSLTemplate;
import hu.dpc.edu.springbootdemo.helper.sql.BookRowMapper;
import hu.dpc.edu.springbootdemo.helper.sql.SQLPredicate;
import hu.dpc.edu.springbootdemo.helper.sql.SQLPredicateBuilder;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

public class JdbcBookRepository implements BookRepository {

  private final static String INSERT_BOOK = "INSERT INTO BOOKS (TITLE, AUTHOR) VALUES (?, ?)";

  private JdbcTemplate jdbcTemplate;
  private QueryDSLTemplate queryDSLTemplate;
  private BookRowMapper rowMapper;

  private SqlUpdate insertBookStatement;

  public JdbcBookRepository(JdbcTemplate jdbcTemplate, BookRowMapper rowMapper) {
    this.jdbcTemplate = jdbcTemplate;
    this.rowMapper = rowMapper;

    insertBookStatement = new SqlUpdate(jdbcTemplate.getDataSource(), INSERT_BOOK);
  }

  @Override
  public long add(Book book) {

    final GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

    insertBookStatement.update(new Object[]{book.getTitle(), book.getAuthor()}, keyHolder);

    return keyHolder.getKey().longValue();
  }

  @Override

  public Book findById(long id) throws EntityNotFoundException {
    try {
      return jdbcTemplate.queryForObject("SELECT ID, TITLE, AUTHOR FROM BOOKS WHERE ID = ?"
              , rowMapper, id);
    } catch (EmptyResultDataAccessException ex) {
      throw new EntityNotFoundException("Book not found with id " + id);
    }
  }

  @Override
  public List<Book> findAll() {
    return jdbcTemplate.query("SELECT ID, TITLE, AUTHOR FROM BOOKS", rowMapper);
  }

  @Override
  public void update(Book book) {
    Objects.requireNonNull(book, "book must not be null");
    Objects.requireNonNull(book.getId(), "book id must not be null");
    final int updatedCount =
            jdbcTemplate.update("UPDATE BOOKS SET TITLE=?, AUTHOR=? WHERE ID=?",
                    book.getTitle(), book.getAuthor(), book.getId());
    if (updatedCount != 1) {
      throw new EntityNotFoundException("Book not found with id " + book.getId());
    }
  }

  @Override
  public List<Book> findAllByExample(Book example) {
    final SQLPredicate predicate = new SQLPredicateBuilder()
            .eq("ID", example.getId())
            .like("TITLE", example.getTitle())
            .like("AUTHOR", example.getAuthor()).build();

    return jdbcTemplate.query(
            "SELECT ID, TITLE, AUTHOR FROM BOOKS" + predicate.whereClause,
            rowMapper,
            predicate.values);
  }
}
