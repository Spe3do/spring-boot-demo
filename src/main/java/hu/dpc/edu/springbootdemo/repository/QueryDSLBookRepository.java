package hu.dpc.edu.springbootdemo.repository;


import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import hu.dpc.edu.springbootdemo.entity.Book;
import hu.dpc.edu.springbootdemo.helper.querydsl.QBooks;
import hu.dpc.edu.springbootdemo.helper.querydsl.QueryDSLTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QueryDSLBookRepository implements BookRepository {


  private QueryDSLTemplate queryDSLTemplate;

  final QBooks books = new QBooks("books");
  final QBean<Book> bookBean = Projections.bean(Book.class, books.id, books.title, books.author);

  public QueryDSLBookRepository(QueryDSLTemplate queryDSLTemplate) {
    this.queryDSLTemplate = queryDSLTemplate;
  }

  @Override
  public long add(Book book) {

    final GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

    return queryDSLTemplate.executeInsert(books, insert ->
            insert.columns(books.title, books.author)
                    .values(book.getTitle(), book.getAuthor())
                    .executeWithKey(books.id));
  }

  @Override

  public Book findById(long id) throws EntityNotFoundException {

    final Book book = queryDSLTemplate.executeQuery(query ->
            query
                    .select(bookBean)
                    .from(books)
                    .where(books.id.eq(id))
                    .fetchOne());
    if (book == null) {
      throw new EntityNotFoundException("Book not found with id " + id);
    }
    return book;

  }

  @Override
  public List<Book> findAll() {
    return queryDSLTemplate.executeQuery(query ->
            query
                    .select(bookBean)
                    .from(books)
                    .fetch());
  }

  @Override
  public void update(Book book) {
    queryDSLTemplate.executeUpdate(books, update ->
            update
                    .set(books.title, book.getTitle())
                    .set(books.author, book.getAuthor()));

  }

  @Override
  public List<Book> findAllByExample(Book example) {

    return queryDSLTemplate.executeQuery(query ->
            query.select(bookBean)
                    .from(books)
                    .where(books.id.eq(example.getId()))
                    .fetch()
    );
  }
}
