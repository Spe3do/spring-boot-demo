package hu.dpc.edu.springbootdemo.entity;

public class Bookmark {
  private String id;
  private String url;
  private String name;
  private Color color;

  public Bookmark(String url, String name, Color color) {
    this.url = url;
    this.name = name;
    this.color = color;
  }

  public String getId() {
    return id;
  }

  public String getUrl() {
    return url;
  }

  public String getName() {
    return name;
  }

  public Color getColor() {
    return color;
  }
}
