package hu.dpc.edu.springbootdemo.helper.querydsl;

import com.querydsl.sql.RelationalPath;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.SQLTemplates;
import com.querydsl.sql.dml.SQLInsertClause;
import com.querydsl.sql.dml.SQLUpdateClause;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Connection;

@Component
public class QueryDSLTemplate {

  private JdbcTemplate jdbcTemplate;
  private SQLTemplates dialect;

  public QueryDSLTemplate(JdbcTemplate jdbcTemplate, SQLTemplates dialect) {
    this.jdbcTemplate = jdbcTemplate;
    this.dialect = dialect;
  }

  public <T> T executeQuery(QueryCallback<T> queryCallback) {
    return jdbcTemplate.execute((Connection connection) ->
            queryCallback.doWithQuery(new SQLQuery<>(connection, dialect)));
  }

  public <T> T executeInsert(RelationalPath<?> entity, InsertCallback<T> queryCallback) {
    return jdbcTemplate.execute((Connection connection) ->
            queryCallback.doWithInsertClause(new SQLInsertClause(connection, dialect, entity)));
  }

  public <T> T executeUpdate(RelationalPath<?> entity, UpdateCallback<T> queryCallback) {
    return jdbcTemplate.execute((Connection connection) ->
            queryCallback.doWithUpdateClause(new SQLUpdateClause(connection, dialect, entity)));
  }

}
