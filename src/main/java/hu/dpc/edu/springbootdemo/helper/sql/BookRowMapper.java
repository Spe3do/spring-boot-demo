package hu.dpc.edu.springbootdemo.helper.sql;


import hu.dpc.edu.springbootdemo.entity.Book;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BookRowMapper implements RowMapper<Book> {
  @Override
  public Book mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    final long bookId = resultSet.getLong("ID");
    final String title = resultSet.getString("TITLE");
    final String author = resultSet.getString("AUTHOR");
    System.out.println(bookId + ", title: " + title + ", author: " + author);
    return new Book(bookId, title, author);
  }
}
