package hu.dpc.edu.springbootdemo.helper.querydsl;

import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.dml.SQLInsertClause;

import java.sql.SQLException;

public interface InsertCallback<R> {
  R doWithInsertClause(SQLInsertClause insertClause) throws SQLException;
}
