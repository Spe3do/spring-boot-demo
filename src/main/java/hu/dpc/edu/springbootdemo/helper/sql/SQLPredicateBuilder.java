package hu.dpc.edu.springbootdemo.helper.sql;

import java.util.ArrayList;
import java.util.List;

public class SQLPredicateBuilder {

  private StringBuilder whereClause;
  private List values = new ArrayList();

  public SQLPredicateBuilder() {
    whereClause = new StringBuilder();
  }

  public SQLPredicateBuilder eq(String field, Object value) {
    if (value != null) {
      appendOrIfNotFirstPredicate();
      whereClause.append(field).append(" = ?");
      values.add(value);
    }
    return this;
  }

  public SQLPredicateBuilder like(String field, String value) {
    if (value != null) {
      appendOrIfNotFirstPredicate();
      whereClause.append(field).append(" LIKE ?");
      values.add("%" + value + "%");
    }
    return this;
  }

  public SQLPredicate build() {
    String whereClauseString = whereClause.length() != 0
            ? " WHERE " + whereClause
            : "";
    return new SQLPredicate(whereClauseString, values.toArray());
  }

  private void appendOrIfNotFirstPredicate() {
    if (whereClause.length() != 0) {
      whereClause.append("\n OR ");
    }
  }

}
