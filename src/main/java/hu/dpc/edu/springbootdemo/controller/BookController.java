package hu.dpc.edu.springbootdemo.controller;

import hu.dpc.edu.springbootdemo.entity.Book;
import hu.dpc.edu.springbootdemo.repository.BookRepository;
import hu.dpc.edu.springbootdemo.repository.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

  @Autowired
  private BookRepository repository;


  @GetMapping()
  public List<Book> findByExample(@RequestParam(required = false) Long id,
                                  @RequestParam(required = false) String title,
                                  @RequestParam(required = false) String author) {
    if (id == null && author == null && title == null) {
      return repository.findAll();
    } else {
      return repository.findAllByExample(new Book(id, title, author));
    }
  }

  @GetMapping("{id}")
  public Book findById(@PathVariable long id) {
    return repository.findById(id);
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public Book add(@RequestBody Book book) {
    final long bookId = repository.add(book);
    book.setId(bookId);
    return book;
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<String> handleEntityNotFoundException(EntityNotFoundException ex) {
    return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .contentType(MediaType.TEXT_PLAIN)
            .body(ex.getMessage());
  }

}
