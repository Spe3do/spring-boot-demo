package hu.dpc.edu.springbootdemo.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class BookmarkController {

  @RequestMapping(method = RequestMethod.GET)
  public ResponseEntity ize() {
    String validationErrors = "x";
    return ResponseEntity.unprocessableEntity()
            .header("X-Error-Code", "VALIDATION_ERROR")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .body(validationErrors);
  }
}
